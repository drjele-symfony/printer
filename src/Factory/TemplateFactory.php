<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\SymfonyPrinter\Factory;

use Drjele\SymfonyPrinter\Exception\NotFoundException;
use Drjele\SymfonyPrinter\Template\AbstractTemplate;

final class TemplateFactory
{
    /** @var AbstractTemplate[][] */
    private array $templates;

    public function __construct(iterable $printerTemplates)
    {
        $this->templates = [];
        /** @var AbstractTemplate $template */
        foreach ($printerTemplates as $template) {
            $this->templates[$template::getEntityClass()] = $this->templates[$template::getEntityClass()] ?? [];

            $this->templates[$template::getEntityClass()][\get_class($template)] = $template;
        }
    }

    public function get(string $templateClass): AbstractTemplate
    {
        foreach ($this->templates as $templates) {
            if (isset($templates[$templateClass])) {
                return $templates[$templateClass];
            }
        }

        throw new NotFoundException(sprintf('could not find template "%s"', $templateClass));
    }

    public function getForEntity($entity): AbstractTemplate
    {
        $entityClass = \get_class($entity);

        $templates = $this->getTemplates($entityClass);
        foreach ($templates as $template) {
            if ($template->supports($entity)) {
                return $template;
            }
        }

        throw new NotFoundException(sprintf('could not find template for "%s"', $entityClass));
    }

    /** @return AbstractTemplate[] */
    public function getTemplates(string $entityClass): array
    {
        if (!isset($this->templates[$entityClass])) {
            throw new NotFoundException(sprintf('no templates exist for "%s"', $entityClass));
        }

        return $this->templates[$entityClass];
    }
}
