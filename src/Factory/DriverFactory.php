<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\SymfonyPrinter\Factory;

use Drjele\SymfonyPrinter\Contract\PrinterInterface;
use Drjele\SymfonyPrinter\Driver\AbstractDriver;
use Drjele\SymfonyPrinter\Exception\Exception;
use Drjele\SymfonyPrinter\Exception\NotFoundException;

final class DriverFactory
{
    private array $drivers;

    public function __construct(iterable $printerDrivers)
    {
        $this->drivers = [];
        /** @var AbstractDriver $driver */
        foreach ($printerDrivers as $driver) {
            if (isset($this->drivers[$driver->getName()])) {
                throw new Exception(sprintf('duplicate driver "%s"', $driver->getName()));
            }

            $this->drivers[$driver->getName()] = $driver;
        }
    }

    public function get(PrinterInterface $printer): AbstractDriver
    {
        if (!isset($this->drivers[$printer->getDriver()])) {
            throw new NotFoundException(sprintf('could not find printer driver "%s"', $printer->getDriver()));
        }

        return $this->drivers[$printer->getDriver()];
    }
}
