<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\SymfonyPrinter\Dto\Transport;

use Generator;

abstract class AbstractDto
{
    private string $host;
    private int $port;
    /** @info the timeout is in seconds */
    private int $timeout;
    private array $data;

    abstract public function getDataToPrint(): Generator;

    final public function __construct(string $host, int $port, int $timeout)
    {
        $this->host = $host;
        $this->port = $port;
        $this->timeout = $timeout;
        $this->data = [];
    }

    final public function getHost(): ?string
    {
        return $this->host;
    }

    final public function getPort(): ?int
    {
        return $this->port;
    }

    final public function getTimeout(): ?int
    {
        return $this->timeout;
    }

    final public function addData(string $data): self
    {
        $this->data[] = $data;

        return $this;
    }

    final protected function getData(): ?array
    {
        return $this->data;
    }
}
