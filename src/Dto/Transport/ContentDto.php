<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\SymfonyPrinter\Dto\Transport;

use Generator;

/** @info this dto contains the direct data to be sent to the printer */
class ContentDto extends AbstractDto
{
    public function getDataToPrint(): Generator
    {
        foreach ($this->getData() as $data) {
            yield $data;
        }
    }
}
