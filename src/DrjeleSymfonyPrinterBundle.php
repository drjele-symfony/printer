<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\SymfonyPrinter;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class DrjeleSymfonyPrinterBundle extends Bundle
{
}
