<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\SymfonyPrinter\Template;

use Drjele\SymfonyPrinter\Contract\PrinterInterface;
use Twig\Environment;

abstract class AbstractTemplate
{
    private Environment $twig;

    abstract public static function getEntityClass(): string;

    abstract protected function getView(PrinterInterface $printer, $entity): array;

    final public function build(PrinterInterface $printer, array $entities): array
    {
        $data = [];

        foreach ($entities as $entity) {
            [$view, $parameters] = $this->getView($printer, $entity);

            $data[] = $this->render($view, $parameters);
        }

        return $data;
    }

    final public function setTwig(Environment $twig): self
    {
        $this->twig = $twig;

        return $this;
    }

    /** @info overwrite for a more complex logic */
    public function supports($entity): bool
    {
        return \get_class($entity) === static::getEntityClass();
    }

    final protected function render(string $view, array $parameters): string
    {
        return $this->twig->render($view, $parameters);
    }
}
