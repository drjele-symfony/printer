<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\SymfonyPrinter\Transport;

use Drjele\SymfonyPrinter\Contract\TransportInterface;
use Drjele\SymfonyPrinter\Dto\Transport\AbstractDto;
use Drjele\SymfonyPrinter\Exception\Exception;
use Throwable;

class TcpTransport implements TransportInterface
{
    public function send(AbstractDto $transportDto): void
    {
        try {
            $stream = $this->open($transportDto);

            foreach ($transportDto->getDataToPrint() as $data) {
                $this->write($stream, $data);
            }

            fclose($stream);
        } catch (Throwable $t) {
            throw new Exception($t->getMessage(), $t->getCode(), $t);
        }
    }

    private function open(AbstractDto $transportDto)
    {
        $stream = @stream_socket_client(
            sprintf('tcp://%s:%s', $transportDto->getHost(), $transportDto->getPort()),
            $errorNumber,
            $errorMessage,
            $transportDto->getTimeout()
        );

        if (false === $stream) {
            $message = sprintf(
                'error connecting to printer "%s:%s", with timeout "%s"',
                $transportDto->getHost(),
                $transportDto->getPort(),
                $transportDto->getTimeout()
            );

            if ($errorNumber) {
                $message = sprintf('%s: [%s] %s', $message, $errorMessage, $errorNumber);
            }

            throw new Exception($message);
        }

        return $stream;
    }

    private function write($stream, string $data): void
    {
        $result = fwrite($stream, $data);

        if ($data && (!$result || $result !== \strlen($data))) {
            throw new Exception('could not write print data to stream');
        }
    }
}
