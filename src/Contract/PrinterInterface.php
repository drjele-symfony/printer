<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\SymfonyPrinter\Contract;

interface PrinterInterface
{
    public const DRIVER_ZEBRA = 'zebra';

    public function getIp(): string;

    public function getDriver(): string;
}
