<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\SymfonyPrinter\Contract;

use Drjele\SymfonyPrinter\Dto\Transport\AbstractDto;

interface TransportInterface
{
    public function send(AbstractDto $transportDto): void;
}
