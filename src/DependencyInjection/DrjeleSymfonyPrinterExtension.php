<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\SymfonyPrinter\DependencyInjection;

use Drjele\SymfonyPrinter\Driver\AbstractDriver;
use Drjele\SymfonyPrinter\Template\AbstractTemplate;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Twig\Environment;

class DrjeleSymfonyPrinterExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../Resources/config')
        );
        $loader->load('services.yaml');

        $container->registerForAutoconfiguration(AbstractDriver::class)
            ->addTag('drjele.printer.driver');

        $container->registerForAutoconfiguration(AbstractTemplate::class)
            ->addTag('drjele.printer.template')
            ->addMethodCall('setTwig', [new Reference(Environment::class)]);
    }
}
