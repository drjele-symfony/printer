<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\SymfonyPrinter\Service;

use Drjele\SymfonyPrinter\Contract\PrinterInterface;
use Drjele\SymfonyPrinter\Dto\Transport\AbstractDto;
use Drjele\SymfonyPrinter\Factory\DriverFactory;
use Drjele\SymfonyPrinter\Factory\TemplateFactory;

class PrintService
{
    private DriverFactory $driverFactory;
    private TemplateFactory $templateFactory;

    public function __construct(DriverFactory $driverFactory, TemplateFactory $templateFactory)
    {
        $this->driverFactory = $driverFactory;
        $this->templateFactory = $templateFactory;
    }

    public function print(PrinterInterface $printer, array $data): AbstractDto
    {
        $driver = $this->driverFactory->get($printer);

        return $driver->print($printer, $data);
    }

    public function printEntities(PrinterInterface $printer, array $entities, int $copies = 1): AbstractDto
    {
        $entitiesData = [];

        foreach ($entities as $entity) {
            $template = $this->templateFactory->getForEntity($entity);

            $data = $template->build($printer, [$entity]);

            $entitiesData = array_merge(
                $entitiesData,
                array_fill(0, $copies, reset($data))
            );
        }

        return $this->print($printer, $entitiesData);
    }
}
