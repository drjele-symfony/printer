<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\SymfonyPrinter\Exception;

class Exception extends \Exception
{
}
