<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\SymfonyPrinter\Driver;

use Drjele\SymfonyPrinter\Contract\PrinterInterface;
use Drjele\SymfonyPrinter\Dto\Transport\AbstractDto;
use Drjele\SymfonyPrinter\Dto\Transport\ContentDto;
use Drjele\SymfonyPrinter\Transport\TcpTransport;

class ZebraDriver extends AbstractDriver
{
    public function getName(): string
    {
        return PrinterInterface::DRIVER_ZEBRA;
    }

    protected function getTransportClass(): string
    {
        return TcpTransport::class;
    }

    protected function getTransportDtoClass(): string
    {
        return ContentDto::class;
    }

    protected function getPort(): int
    {
        return 9100;
    }

    protected function getTimeout(): int
    {
        return 5;
    }

    protected function addData(AbstractDto $transportDto, array $data): void
    {
        $transportDto->addData(implode('', $data));
    }
}
