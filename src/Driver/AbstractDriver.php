<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\SymfonyPrinter\Driver;

use Drjele\SymfonyPrinter\Contract\PrinterInterface;
use Drjele\SymfonyPrinter\Contract\TransportInterface;
use Drjele\SymfonyPrinter\Dto\Transport\AbstractDto;

abstract class AbstractDriver
{
    abstract public function getName(): string;

    abstract protected function getTransportClass(): string;

    abstract protected function getTransportDtoClass(): string;

    abstract protected function getPort(): int;

    abstract protected function getTimeout(): int;

    abstract protected function addData(AbstractDto $transportDto, array $data): void;

    final public function print(PrinterInterface $printer, array $data): AbstractDto
    {
        $transportDto = $this->buildTransportDto($printer, $data);

        $this->send($transportDto);

        return $transportDto;
    }

    private function send(AbstractDto $transportDto): void
    {
        $transportClass = $this->getTransportClass();

        /** @var TransportInterface $transport */
        $transport = new $transportClass();

        $transport->send($transportDto);
    }

    private function buildTransportDto(PrinterInterface $printer, array $data): AbstractDto
    {
        $transportDto = $this->getTransportDto($printer);

        $this->addData($transportDto, $data);

        return $transportDto;
    }

    private function getTransportDto(PrinterInterface $printer): AbstractDto
    {
        $contentDtoClass = $this->getTransportDtoClass();

        return new $contentDtoClass($printer->getIp(), $this->getPort(), $this->getTimeout());
    }
}
