# Symfony Printer

**You may fork and modify it as you wish**.

Any suggestions are welcomed.

## Usage

* **\Drjele\SymfonyPrinter\Service\PrintService::print** - send an array of data/commands to the printer.
* **\Drjele\SymfonyPrinter\Service\PrintService::printEntities** - print entities based on template generation.
* **\Drjele\SymfonyPrinter\Template\AbstractTemplate** - extend this to create templates.
